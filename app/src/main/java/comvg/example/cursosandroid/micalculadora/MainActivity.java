package comvg.example.cursosandroid.micalculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private EditText txtNum1;
    private EditText txtNum2;
    private EditText txtResultado;
    private Button btnSuma;
    private Button btnResta;
    private Button btnMult;
    private Button btnDivi;
    private Button btnLimpiar;
    private Button btnCerrar;

    private Operaciones operaciones = new Operaciones();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initComponents();

        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    public void initComponents(){
        txtNum1 = findViewById(R.id.txtNum1);
        txtNum2 = findViewById(R.id.txtNum2);
        txtResultado = findViewById(R.id.txtResult);
        btnSuma = findViewById(R.id.btnSuma);
        btnResta = findViewById(R.id.btnResta);
        btnMult = findViewById(R.id.btnMult);
        btnDivi = findViewById(R.id.btnDivi);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnCerrar = findViewById(R.id.btnCerrar);
        setEventos();
    }

    public void setEventos(){
        this.btnSuma.setOnClickListener(this);
        this.btnResta.setOnClickListener(this);
        this.btnMult.setOnClickListener(this);
        this.btnDivi.setOnClickListener(this);
        this.btnLimpiar.setOnClickListener(this);
        this.btnCerrar.setOnClickListener(this);
    }

    public boolean camposLlenos(){
        if (txtNum1.getText().toString().equals("") || txtNum2.getText().toString().equals("")){
            return false;
        }else{
            return true;
        }
    }

    public void limpiarCampos(){
        txtNum1.setText("");
        txtNum2.setText("");
        txtResultado.setText("");
        txtNum1.requestFocus();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.btnSuma: //Sumar
                if(camposLlenos()){
                    operaciones.setNum1(Float.parseFloat(txtNum1.getText().toString()));
                    operaciones.setNum2(Float.parseFloat(txtNum2.getText().toString()));
                    txtResultado.setText(String.valueOf(operaciones.suma()));
                }else{
                    Toast.makeText(MainActivity.this,"Complete los campos",Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.btnResta: //Restar
                if(camposLlenos()){
                    operaciones.setNum1(Float.parseFloat(txtNum1.getText().toString()));
                    operaciones.setNum2(Float.parseFloat(txtNum2.getText().toString()));
                    txtResultado.setText(String.valueOf(operaciones.resta()));
                }else{
                    Toast.makeText(MainActivity.this,"Complete los campos",Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.btnMult: //Multiplicar
                if(camposLlenos()){
                    operaciones.setNum1(Float.parseFloat(txtNum1.getText().toString()));
                    operaciones.setNum2(Float.parseFloat(txtNum2.getText().toString()));
                    txtResultado.setText(String.valueOf(operaciones.mult()));
                }else{
                    Toast.makeText(MainActivity.this,"Complete los campos",Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.btnDivi: //Dividir
                if(camposLlenos()){
                    operaciones.setNum1(Float.parseFloat(txtNum1.getText().toString()));
                    operaciones.setNum2(Float.parseFloat(txtNum2.getText().toString()));
                    txtResultado.setText(String.valueOf(operaciones.div()));
                }else{
                    Toast.makeText(MainActivity.this,"Complete los campos",Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.btnLimpiar: //Limpiar
                if(camposLlenos())
                    limpiarCampos();
                else
                    Toast.makeText(MainActivity.this,"Nada para limpiar",Toast.LENGTH_LONG).show();
                break;
            case R.id.btnCerrar: //Cerrar
                finish();
                break;
        }

    }
}
